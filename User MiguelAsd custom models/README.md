This is a custom model, with different servo configuration.

All the 3D printed parts weight about 300-400 grams.

Bolts i used:


- M3 x45 mm: quantity 12
    
- M3 x30 mm: quantity 26
    
- M3 x25 mm: quantity 16
    
- M3 x20 mm: quantity 8
    
- M3 x15 mm: quantity 16
    
- M3 x7 mm: quantity 4
    
- M3 threaded rod x 42 mm: quantity 4
    
- M3 threaded rod x 74 mm: quantity 8
    

Here is a video showing how i built it:
https://youtu.be/HeP_GRduamE

![Logo](front.jpg)
![Logo](side.jpg)
![Logo](back.jpg)